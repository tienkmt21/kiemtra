package Cau5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectSQL {
    private static final String URL = "jdbc:mariadb://%s:3306/%s";
    private static String user = "root";
    private static String password = "12345678";

    public static Connection createConnection() throws SQLException {

        return DriverManager.getConnection(String.format(URL, "localhost", "cau4"), user, password);
    }
}
