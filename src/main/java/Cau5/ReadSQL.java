package Cau5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ReadSQL {
    public static void main(String[] args) throws SQLException {
        Connection connection = ConnectSQL.createConnection();
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM `user`");
        ResultSet res = ps.executeQuery();
        while (res.next()) {
            System.out.printf("username : %s, avatar : %s, email : %s, fullname : %s, updateat : %s, status : %d, source : %s, password : %s \n",
                    res.getString(1),
                    res.getString(2),
                    res.getString(3),
                    res.getString(4),
                    res.getString(5),
                    res.getByte(6),
                    res.getString(7),
                    res.getString(8));
        }
    }
}
