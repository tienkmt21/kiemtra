package Cau3;

import java.io.*;

public class ReadFile {
    public static void main(String[] args) {
        try {
            File files = new File("read.txt");
            FileReader fileReader = new FileReader(files);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File not found!");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
