CREATE DATABASE IF NOT EXISTS `cau4`;
USE `cau4`;

CREATE TABLE IF NOT EXISTS `user` (
  `usename` varchar(25) DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `updateat` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` tinyint(1) DEFAULT NULL,
  `source` varchar(15) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
