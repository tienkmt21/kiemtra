package Cau2;

public class Reverse {
    public static void main(String[] args) {
        String input = "java developer";
        reverse(input);
    }

    static void reverse(String input) {
        StringBuilder stringBuilder = new StringBuilder(input);
        String temp = stringBuilder.reverse().toString();
        char[] result = temp.toCharArray();
        char[] output = new char[result.length];
        for (int i = 0; i < result.length; i++) {
            if (result[i] == 97 || result[i] == 101 || result[i] == 105 || result[i] == 111 || result[i] == 117) {
                output[i] = (char) (result[i] - 32);
            } else {
                output[i] = result[i];
            }
        }
        System.out.println(output);
    }
}
